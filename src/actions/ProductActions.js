/**
 * @action        : ProductActions.js
 * @description   : Handles product fetch
 * @Created by    : Puneet
 */

import { FETCH_PRODUCTS, productActionTypes } from "./constants";
import { checkHttpStatus, getSuccess } from "./../utils/";


export const fetchProducts = () => {
  
  return (dispatch) => {

    fetch(`${FETCH_PRODUCTS}`, {
      method: "GET",
    })
      .then(checkHttpStatus)
      .then(function(response) {
        dispatch(getSuccess(productActionTypes.product_list.SUCCESS, response));
      })
      .catch(function(error) {
        // errorHandler(error);
      });

  };


};
