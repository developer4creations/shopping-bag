import { createRequestActionTypes } from "./../../utils/";

export const FETCH_PRODUCTS = "https://api.myjson.com/bins/19ynm&callback=callbackFN3";

export const productActionTypes = {
  product_list: createRequestActionTypes("PRODUCTS"),
};
