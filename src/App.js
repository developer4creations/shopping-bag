/*eslint no-script-url: 0*/
import React, { Component } from "react";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleState: "",
    };
  }

  componentDidMount() {
    document.title = "Sapient Code Exercise";
  }

  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

export default App;
