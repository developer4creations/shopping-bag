/**
 * @reducer       : index reducer
 * @description   :
 * @Created by    : Puneet
 */

import { combineReducers } from "redux";
import ProductReducer from "./ProductReducer";

import { reducer as formReducer } from "redux-form"; // SAYING use redux form reducer as reducer

const rootReducer = combineReducers({
  form: formReducer,
  product: ProductReducer,
});

export default rootReducer;
