import { productActionTypes } from "./../actions/constants";

const initialState = {
  product_list: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
  case productActionTypes.product_list.REQUEST:
    return {};
  case productActionTypes.product_list.SUCCESS:
    return {
      ...state,
      product_list: payload.productsInCart,
      loading: false,
    };
  default:
    return state;
  }
};
