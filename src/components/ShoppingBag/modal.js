/* eslint no-script-url: 0*/
import React, { Component } from "react";

class Modal extends Component {
  constructor(props) {
    super(props);

    let selected_color = this.props.product_data.p_selected_color.name;
    let color_found = false;
    const newData = this.props.product_data.p_available_options.colors.map((obj, index) => {
      const newObj = Object.assign({}, obj);
      newObj.class = "colors ";
      if (newObj.name === selected_color) {
        newObj.class = "colors selectColor";
        color_found = true;
      }
      return newObj;
    });
    if (!color_found) {
      newData[0].class = "colors selectColor";
      selected_color = newData[0].name;
    }
    this.state = {
      product_colors: newData,
      selected_size: this.props.product_data.p_selected_size.code,
      product_quantity: this.props.product_data.p_quantity,
      product_data: this.props.product_data,
      selected_color: selected_color,
      p_price: this.props.product_data.p_price,
      button_text: "ADD TO BAG",
    };

    this.updateSelectedColor = this.updateSelectedColor.bind(this);
    this.onSizeChange = this.onSizeChange.bind(this);
    this.onQuantityChange = this.onQuantityChange.bind(this);
    this.updateProductDetail = this.updateProductDetail.bind(this);
  }

  updateSelectedColor(event) {
    let selected_color = event.target.id;
    const newData = this.props.product_data.p_available_options.colors.map((obj, index) => {
      const newObj = Object.assign({}, obj);
      newObj.class = "colors ";
      if (newObj.name === selected_color) {
        newObj.class = "colors selectColor";
      }
      return newObj;
    });
    this.setState({
      product_colors: newData,
      selected_color: selected_color,
    });
  }

  updateProductDetail() {
    this.setState({
      p_price: this.state.product_quantity * this.state.product_data.p_originalprice,
    });
  }

  renderColors() {
    let color_options = this.state.product_colors;
    let classInstance = this;
    return color_options.map((color, index) => {
      return <span key={index} onClick={classInstance.updateSelectedColor} id={color.name} className={color.class} style={{ backgroundColor: color.hexcode }} ></span>;
    });
  }

  renderSizes() {
    let size_options = this.props.product_data.p_available_options.sizes;
    return size_options.map((sizeObj, index) => {
      return <option key={index} value={sizeObj.code}>{sizeObj.name.toUpperCase()}</option>;
    });
  }

  onSizeChange(event) {
    this.setState({ selected_size: event.target.value });
    let classInstance = this;
    setTimeout(function() {
      classInstance.updateProductDetail();
    }, 200);
  }

  onQuantityChange(event) {
    this.setState({ product_quantity: event.target.value });
    let classInstance = this;
    setTimeout(function() {
      classInstance.updateProductDetail();
    }, 200);
  }

  updateBagButton() {
    let product = this.state.product_data;
    let classInstance = this;
    product.p_quantity = this.state.product_quantity;
    product.p_price = this.state.product_quantity * product.p_originalprice;
    product.p_selected_size.code = this.state.selected_size;
    product.p_selected_color.name = this.state.selected_color;
    this.setState({
      product_data: {},
      p_price: this.state.product_quantity * this.state.product_data.p_originalprice,
      button_text: "Updating...",
    });
    setTimeout(function() {
      classInstance.props.onClickClosePopup();
    }, 100);
  }

  render() {
    const { product_data } = this.state;
    return (
      <div id="popup" className="modal-box" >
        {product_data.p_name ?
          <div>
            <a href="javascript:void(0)" className="js-modal-close close" onClick={this.props.onClickClosePopup}>×</a>
            <div className="modal-body">
              <div className="cartTable">
                <div className="cartContent floatLeft">
                  <div className="itemDetails center">
                    <div className="productName">{product_data.p_name.toUpperCase()}</div>
                    <div className="productPrice"><span><sup>$</sup>{this.state.p_price}</span></div>
                    <div className="productColors">
                      {this.renderColors()}
                    </div>
                    <div className="productSizes">
                      <select value={this.state.selected_size} onChange={this.onSizeChange} className="availableSizes">
                        <option value="0"> SIZE </option>
                        {this.renderSizes()}
                      </select>
                      <select className="availableQty" value={this.state.product_quantity} onChange={this.onQuantityChange}>
                        <option value="0"> Qty </option>
                        <option value="1"> 1 </option>
                        <option value="2"> 2 </option>
                        <option value="3"> 3 </option>
                        <option value="4"> 4 </option>
                        <option value="5"> 5 </option>
                        <option value="6"> 6 </option>
                        <option value="7"> 7 </option>
                        <option value="8"> 8 </option>
                        <option value="9"> 9 </option>
                        <option value="10"> 10 </option>
                      </select>
                    </div>
                    <input type="hidden" id="itemCart" value="" />
                    <div className="productBtn">
                      <button className="blueBtn floatLeft" onClick={this.updateBagButton.bind(this)} >{this.state.button_text}</button>
                    </div>
                    <div> <a href="javascript:void(0)">See product details. </a></div>
                  </div>
                </div>
                <a href="javascript:void(0)">
                  <div className="cartItemImg floatRight">
                    <img alt={product_data.p_name.toUpperCase()} src={require(`../../assets/P${product_data.p_id}.jpg`)} />
                  </div>
                </a>
              </div>
            </div>
          </div>
          : null }
      </div>
    );
  }
}

export default Modal;
