/* eslint no-script-url: 0*/
import React, { Component } from "react";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import "./../../assets/bag.css";
import { Header } from "./../common/header";
import { Footer } from "./../common/footer";
import { fetchProducts } from "./../../actions/ProductActions";
import Product from "./list";
import Modal from "./modal";

class ShoppingBag extends Component {

  constructor(props) {
    super(props);
    this.state = {
      product_list: [],
      itemSubTotal: 0,
      discount: 0,
      estimated_total_value: 0,
      showPopup: false,
      product_detail: {},
    };
    this.onQuantityChange = this.onQuantityChange.bind(this);
    this.onRemoveClick = this.onRemoveClick.bind(this);
    this.onClickOpenModal = this.onClickOpenModal.bind(this);
    this.onClosePopup = this.onClosePopup.bind(this);
  }

  componentWillMount() {
    this.props.fetchProducts();
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.product_list && this.props.product_list !== nextProps.product_list) {
      this.calculatePriceAndDiscount(nextProps.product_list);
    }

  }

  renderLists() {
    let thisObj = this;
    return this.state.product_list.map(function(data, rowIndex) {
      return (<Product key={rowIndex} onRemove={thisObj.onRemoveClick} index={rowIndex} detail={data} onQuantityChange={thisObj.onQuantityChange} onClickOpenModal={thisObj.onClickOpenModal} />);
    });
  }

  onClickOpenModal(event) {
    var product = this.state.product_list.filter(function(obj) {
      return obj.p_id === event.target.id;
    });
    this.setState({ showPopup: true, product_detail: product[0] });
  }
  onClosePopup() {
    this.setState({ showPopup: false, product_list: this.state.product_list });
    this.calculatePriceAndDiscount(this.state.product_list);
  }

  calculatePriceAndDiscount(products) {
    let subtotal_price = 0;
    products.map((obj) => {
      return subtotal_price += this.isNumeric(obj.p_quantity) ? obj.p_quantity * obj.p_originalprice : obj.p_originalprice;
    });
    let discount_ercentage = this.discountPercentage(products.length);
    let discount_value = (subtotal_price * discount_ercentage) / 100;
    let final_discount = parseFloat(discount_value).toFixed(2);
    let estimated_total_value = subtotal_price - final_discount;
    estimated_total_value = parseFloat(estimated_total_value).toFixed(2);
    this.setState({
      product_list: products, itemSubTotal: subtotal_price, discount: final_discount,
      estimated_total_value: estimated_total_value,
    });
  }

  onRemoveClick(event) {
    let products = this.state.product_list;
    for (let i = products.length - 1; i >= 0; i--) {
      if (products[i].p_id === event.target.id) products.splice(i, 1);
    }
    this.calculatePriceAndDiscount(products);
  }

  isNumeric(number) {
    return !isNaN(parseFloat(number)) && isFinite(number);
  }

  discountPercentage(totalItems) {
    let discount = 0;
    if (totalItems === 3) {
      discount = 5;
    } else if (totalItems >= 3 && totalItems <= 6) {
      discount = 10;
    } else if (totalItems > 10) {
      discount = 25;
    }
    return discount;
  }

  onQuantityChange(event) {
    let product_id = event.target.id;
    let quantity = event.target.value;

    const newData = this.state.product_list.map((obj) => {
      const newObj = Object.assign({}, obj);
      let item_price = newObj.p_originalprice;

      // update the new object
      if (newObj.p_id === product_id) {
        newObj.p_quantity = quantity;
        item_price = this.isNumeric(quantity) ? quantity * newObj.p_originalprice : newObj.p_originalprice;
      }

      newObj.p_price = item_price;
      return newObj;
    });

    this.calculatePriceAndDiscount(newData);
  }

  render() {console.log(this.state);
    return (
      <div>
        <div className="mainContent">
          <Header item_count={this.state.product_list.length} />
          <div className="cart cartContent">
            {
              this.state.product_list.length > 0 ?
                this.renderLists() : ""
            }
          </div>
          <div className="checkoutSection clear">
            <div className="leftContainer">
              <p><strong>Need help or have any questions?</strong></p>
              <p> Call customer service at: 1-800-555-55555</p>
              <p><a href="javascript:void(0);" title="Chat with one of our stylists."> Chat with one of our stylists.</a></p>
              <p></p>
              <p><a href="javascript:void(0)" title="See return &amp; exchange policy"> See return &amp; exchange policy</a></p>
              <p></p>
            </div>
            <div className="rightContainer">
              <div className="promotionalCodeBlock">
                <div className="codeDetails">
                  <label htmlFor="promotionCode"> ENTER PROMOTION CODE OR GIFT CARD</label>
                </div>
                <span className="promotionCodeInput">
                  <input id="promotionCode" type="text" name="promotionCode" className="input promotionCode" />
                  <button> APPLY</button>
                </span>
              </div>
              <div className="clear lineHeightBlock">
                <div>SUBTOTAL
                  <span id="subTotal" data-subtotal="59" className="floatRight">
                    <sup>$</sup>{this.state.itemSubTotal.toFixed(2)}
                  </span>
                </div>
                <div className="promotion">
                  {
                    this.state.discount !== "0.00" ?
                      (
                        <div className="floatLeft priceCalc"><p>PROMOTION CODE <strong>JF10</strong> APPLIED</p></div>
                      ) : (<div className="floatLeft priceCalc">PROMOTION DISCOUNT</div>)
                  }
                  <div className="floatRight discountheight">- <sup>$</sup>{this.state.discount}</div>
                </div>
                <div>
                  <div className="floatLeft priceCalc">
                    <p>ESTIMATED SHIPPING*</p>
                    <p className="helpText">You qualify for free shipping because your order is over $50*</p>
                  </div>
                  <div className="floatRight discountPrices"> FREE</div>
                </div>
              </div>
              <hr className="lineSeparator" />
              <div className="estimatedTotalBlock clear estimatedShipping">
                <div className="floatLeft">
                  <p>ESTIMATED TOTAL</p>
                  <p className="helpText">Tax will be applied during checkout</p>
                </div>
                <div className="floatRight estimatedTotal"><sup>$</sup>{this.state.estimated_total_value}</div>
              </div>
              <hr className="clear lineSeparatorBroader" />
              <Footer />
            </div>
          </div>
        </div>
        {this.state.showPopup ? (<Modal onClickClosePopup={this.onClosePopup} product_data={this.state.product_detail} />) : ""}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    product_list: state.product.product_list,
  };
}

// connect: first agrument is mapStateToProps, 2nd argument is mapDispatchToProps
ShoppingBag = reduxForm({
  form: "ShoppingBag",
})(ShoppingBag);

export default connect(mapStateToProps, { fetchProducts })(ShoppingBag);
