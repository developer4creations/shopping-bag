/*eslint no-script-url: 0*/
import React from "react";


const Product = ({ detail, index, onQuantityChange, onRemove, onClickOpenModal }) => {
  return (
    <div className="cartItems">
      <div className="cartCell imageContainer">
        <img src={require(`../../assets/P${detail.p_id}.jpg`)} alt="Tshirt 1" title="Tshirt 1" />
      </div>
      <div className="cartCell itemInfo">
        <div className="innerCartCell">
          <div className="details floatLeft">
            <p>{detail.p_name.toUpperCase()}</p>
            <p> Style #: {detail.p_style.toUpperCase()}</p>
            <p> Color: &nbsp;<span className="colorName">{detail.p_selected_color.name}</span></p>
          </div>
          <div className="measures">
            <div className="size floatLeft">{detail.p_selected_size.code.toUpperCase()}</div>
            <div className="qty floatLeft">
              <input type="text" name="qty" id={detail.p_id} value={detail.p_quantity} maxLength="1" onChange={onQuantityChange} className="inputSize" />
            </div>
            <div className="price floatLeft">
              <sup>{detail.c_currency}</sup>
              <span className="calcPrice">{detail.p_price.toFixed(2)}</span>
            </div>
          </div>
        </div>
        <div className="clear controls">
          <a href="javascript:void(0)" id={detail.p_id} title="Edit" onClick={onClickOpenModal}> EDIT</a>
          <span className="separator"></span>
          <a href="javascript:void(0)" title="Remove" id={detail.p_id} onClick={onRemove}>
            <span className="closeBtn">X</span>  REMOVE
          </a>
          <span className="separator"></span>
          <input type="hidden" value="" className="itemCart" />
          <a href="javascript:void(0)" title="Save for later" onClick=""> SAVE FOR LATER</a>
        </div>
      </div>
      
    </div>
  );
};
export default Product;
