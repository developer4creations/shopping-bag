/*eslint no-script-url: 0*/
import React from "react";

export const Footer = () => {
  return (
    <div className="checkoutBlock">
      <a href="javascript:void(0)" title=""> CONTINUE SHOPPING</a>
      <button className="blueBtn"> CHECKOUT</button>
      <div className="safeCheckout">
        <img src={require("../../assets/lock.jpg")} alt="Safe checkout" title="Safe checkout" />
        <label> Secure checkout. Shopping is always safe and secure.</label>
      </div>
    </div>
  );

};
