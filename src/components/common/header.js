import React from "react";

export const Header = ({ item_count }) => {
  return (
    <div>
      <div className="header">
        <h1>YOUR SHOPPING BAG<span className="itemCounter floatRight">{item_count} ITEMS</span></h1>
      </div>
      <div className="cart cartHeading">
        <div className="cartItems">
          <div className="cartCell imageContainer">{item_count} ITEMS</div>
          <div className="cartCell itemInfo">
            <div className="innerCartCell">
              <div className="details floatLeft">
              </div>
              <div className="size floatLeft"> SIZE</div>
              <div className="qty floatLeft"> QTY</div>
              <div className="price floatLeft"> PRICE</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );

};
