import React from "react";
import { browserHistory, Route, Router } from "react-router";

// Import components to route
import ShoppingBag from "../components/ShoppingBag";

export default(
  <Router history={browserHistory}>
    <Route path="/" component={ShoppingBag}></Route>
    <Route path="*" component={ShoppingBag}/>
  </Router>
);
